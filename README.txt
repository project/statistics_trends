The statistics_trends module provides a block showing recent hits
to the site over various periods.

Installation

Place the statistics_trends folder in the modules directory of your 
Drupal installation, and enable it in the administration tools. Then
use the blocks configuration for "Statistics Trends" to determine
what periods of history to display.

Credits

Mike Ryan is the author and maintainer of this module. Concept and
original code based on the "Hits by month" block (http://drupal.org/node/17138)
authored by Khalid Baheyeldin (http://baheyeldin.com).